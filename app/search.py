from flask import current_app

def add_to_index(index, model):
    if not current_app.elasticsearch:
        return
    payload = {}
    for field in model.__searchable__:
        payload[field] = getattr(model, field)
    current_app.elasticsearch.index(index=index, id=model.id, body=payload)

def remove_from_index(index, model):
    if not current_app.elasticsearch:
        return
    current_app.elasticsearch.delete(index=index, id=model.id)

def query_index(index, query, page, per_page):
    if not current_app.elasticsearch:
        return [], 0
    search = current_app.elasticsearch.search(
        index=index,
        body={'query': {'multi_match': {'query': query, 'fields': ['*']}},
              'from': (page - 1) * per_page, 'size': per_page})
    ids = [int(hit['_id']) for hit in search['hits']['hits']]
    return ids, search['hits']['total']['value']

# from app.search import add_to_index, remove_from_index, query_index
# for post in Post.query.all():
#     add_to_index('posts', post)
# query_index('posts', 'one two three four five', 1, 100)
# ([15, 13, 12, 4, 11, 8, 14], 7)
# query_index('posts', 'one two three four five', 1, 3)
# ([15, 13, 12], 7)
# query_index('posts', 'one two three four five', 2, 3)
# ([4, 11, 8], 7)
# query_index('posts', 'one two three four five', 3, 3)
# ([14], 7)